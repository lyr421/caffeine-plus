# caffeine-plus

## 说明
[caffeine](https://github.com/ben-manes/caffeine)这里不做过多说明，想了解的可以直接点击名字链接跳转github查看。

## CP起源

### caffeine不能满足要求
caffeine框架在使用之前需要进行缓存参数配置，但默认的参数配置方案都不是很友好。

#### 配置文件方式
不够灵活，多缓存配置文件很长。

- properties
```properties
spring.cache.cache-names=cacheOne
spring.cache.caffeine.spec=initialCapacity=50,maximumSize=500,expireAfterWrite=10s
```

- Yaml文件
```yaml
spring:
  cache:
    type: caffeine
    cache-names:
    - cacheOne
    caffeine:
      spec: maximumSize=5000,refreshAfterWrite=60s
```

#### 配置类方式
不够配置化，增删缓存需要硬编码。

```java
@Configuration
public class CacheConfig {

    /**
     * 创建基于Caffeine的Cache Manager
     *        
     * @return
     */
    @Bean
    @Primary
    public CacheManager caffeineCacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        ArrayList<CaffeineCache> caches = Lists.newArrayList();
        List<CacheObject> list = setCacheObjects();
        for(CacheObject cacheObject : list){
            caches.add(new CaffeineCache(cacheObject.getKey(),
                    Caffeine.newBuilder().recordStats()
                            .expireAfterWrite(cacheObject.getTtl(), TimeUnit.SECONDS)
                            .maximumSize(cacheObject.getMaximumSize())
                            .build()));
        }
        cacheManager.setCaches(caches);
        return cacheManager;
    }


    /**
     * 添加缓存对象
     *
     * @return
     */
    private List<CacheObject> setCacheObjects(){

        List<CacheObject> list = Lists.newArrayList();
        CacheObject userCache = new CacheObject();
        userCache.setKey("userCache");
        userCache.setTtl(60);
        userCache.setMaximumSize(10000);

        CacheObject sysCfnCache = new cacheObject();
        sysCfnCache.setKey("sysCfnCache");
        sysCfnCache.setTtl(60);
        sysCfnCache.setMaximumSize(10000);

        list.add(userCache);
        list.add(deptCache);

        return list;
    }

    @Data    
    class CacheObject {
        private String key;
        private long ttl;
        private long maximumSize;
    }

}
```

### 升级版 caffeine-plus 诞生

#### 特点
- 结合配置文件和配置类灵活配置缓存
- 提供常用方法工具类 CacheUtil
- 与caffeine共存，互为补充
- spring boot auto config自动扫描配置

#### 使用要求
- 0.1版本仅支持spring boot项目
- JDK1.8+
- 配置文件限定只能使用路径`resources/config/cache.yml`

#### 使用方法
##### 配置文件示例
```yaml
cache:
  caffeine-plus:
    # 需要配置true启用缓存，默认为false
    enabled: true
    # 全局配置
    cacheName: beanCache,beanCache2
    spec: maximumSize=500,expireAfterWrite=60s
    # 自定义配置，cacheName相同会覆盖全局配置
    configs:
      - cacheName: sessionCache
        spec: maximumSize=200
      - cacheName: userCache
        spec: maximumSize=200
```
cacheName 为缓存bean名称  
spec 为缓存初始化参数，参数名同[caffeine](https://github.com/ben-manes/caffeine)框架参数
> **Caffeine常用配置说明：**
> 
> initialCapacity=[integer]: 初始的缓存空间大小
> 
> maximumSize=[long]: 缓存的最大条数
> 
> maximumWeight=[long]: 缓存的最大权重
> 
> expireAfterAccess=[duration]: 最后一次写入或访问后经过固定时间过期
> 
> expireAfterWrite=[duration]: 最后一次写入后经过固定时间过期
> 
> refreshAfterWrite=[duration]: 创建缓存或者最近一次更新缓存后经过固定的时间间隔，刷新缓存
> 
> weakKeys: 打开key的弱引用
> 
> weakValues：打开value的弱引用
> 
> softValues：打开value的软引用
> 
> recordStats：开发统计功能
> 
> **注意：**
> 
> expireAfterWrite和expireAfterAccess同时存在时，以expireAfterWrite为准。
> 
> maximumSize和maximumWeight不可以同时使用
> 
> weakValues和softValues不可以同时使用

##### 注解使用
cache方面的注解主要有以下5个：

- @Cacheable 触发缓存入口（这里一般放在创建和获取的方法上，@Cacheable注解会先查询是否已经有缓存，有会使用缓存，没有则会执行方法并缓存）
- @CacheEvict 触发缓存的eviction（用于删除的方法上）
- @CachePut 更新缓存且不影响方法执行（用于修改的方法上，该注解下的方法始终会被执行）
- @Caching 将多个缓存组合在一个方法上（该注解可以允许一个方法同时设置多个注解）
- @CacheConfig 在类级别设置一些缓存相关的共同配置（与其它缓存配合使用）

**说一下@Cacheable 和 @CachePut的区别：**
- @Cacheable：它的注解的方法是否被执行取决于Cacheable中的条件，方法很多时候都可能不被执行。
- @CachePut：这个注解不会影响方法的执行，也就是说无论它配置的条件是什么，方法都会被执行，更多的时候是被用到修改上。

##### 工具类使用
下边提供常用方法示例，其他方法可参考项目源码。
```java
@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class CacheTest {

    @Autowired
    private CacheUtil cacheUtil;

    @Autowired
    private ConfigSystemInfoService configSystemInfoService;

    @Test
    public void testExpireCache() {

        // 查询数据库
        log.info(configSystemInfoServiceImpl.selectByCode("001").getValue());

        // 查询缓存, 验证注解缓存生效
        log.info(configSystemInfoServiceImpl.selectByCode("001").getValue());

        // 主动使用缓存
        cacheUtil.putCache("beanCache", "sex", "female");
        log.info("{}", cacheUtil.getCacheValue("beanCache", "sex"));

        // 主动更新缓存
        cacheUtil.putCache("beanCache", "sex", "male");
        log.info("{}", cacheUtil.getCacheValue("beanCache", "sex"));

        // 睡眠时间大于缓存有效时间
        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 查询数据库，验证缓存失效
        log.info(configSystemInfoServiceImpl.selectByCode("001").getValue());

        // 缓存超时，查询结果为null
        log.info("{}", cacheUtil.getCacheValue("beanCache", "sex"));

        // 新增缓存，查询正常
        cacheUtil.putCache("beanCache", "sex", "male");
        log.info("{}", cacheUtil.getCacheValue("beanCache", "sex"));
    }

}
```

## 结束语
关于caffeine缓存的个人认知
1. 一个缓存name对应的缓存对象是size大小的对象集合  
> 比如beanCache，可以用来存500bean对象，或者200个bean对象+300字符串，可以混用。  
**不建议混用：** 因为此时缓存的性能参数，不能有效作为bean的命中率参考。

2. 永久有效且不回收的缓存，可以作为类似session管理使用  
> session管理的同时，具有caffeine缓存动态更新、高并发等特性。