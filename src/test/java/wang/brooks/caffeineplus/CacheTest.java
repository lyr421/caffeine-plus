package wang.brooks.caffeineplus;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.test.context.SpringBootTest;
import wang.brooks.caffeineplus.config.CacheProperties;
import wang.brooks.caffeineplus.util.CacheUtil;

/**
 * @author: wangyong
 * @date: 2020/1/14
 * <p>
 * @description:
 */

@SpringBootTest(classes = {ApplicationRunner.class})
@Slf4j
public class CacheTest {

    @Autowired
    CacheProperties cacheProperties;

    @Autowired
    private CacheUtil cacheUtil;

    @Test
    public void testCache() {
        cacheProperties.getCacheName().forEach(name -> log.info(name));
        cacheUtil.putCache("userCache", "hello", "hello, brooks");
        log.info("{}", cacheUtil.getCacheValue("userCache", "hello"));
    }

}
