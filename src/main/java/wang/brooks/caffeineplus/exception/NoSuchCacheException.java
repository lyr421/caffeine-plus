package wang.brooks.caffeineplus.exception;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 自定义运行时异常
 * <p>
 * 不存在缓存key时，使用此异常
 *
 * @author: wangyong
 * @date: 2020/1/10
 * <p>
 * @description:
 */
@Slf4j
public class NoSuchCacheException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    @Getter
    private String code = "-1";

    public NoSuchCacheException(String message) {
        super(message);
        log.debug(message);
    }

    public NoSuchCacheException(String code, String message) {
        super(message);
        this.code = code;
    }

}
