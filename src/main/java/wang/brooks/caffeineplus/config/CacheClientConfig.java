package wang.brooks.caffeineplus.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 结合{@link resources/META_INF/spring.factories"}文件
 * <p>
 * 引入项目自动扫描缓存组件包和配置文件
 * 引入端不用在上下文中加 {@link ComponentScan}注解
 *
 * @author: wangyong
 * @date: 2020/1/10
 * <p>
 * @description:
 */

@Configuration
@ComponentScan("wang.brooks.caffeineplus")
@PropertySource(value = {"classpath:config/cache.yml", "file:${spring.profiles.path}/cache.yml"}, encoding = "utf-8",
        ignoreResourceNotFound = true, factory = YamlConfigFactory.class)
public class CacheClientConfig {
}
