package wang.brooks.caffeineplus.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 初始化缓存参数
 * 通过 {@link ConfigurationProperties}注解实现配置文件参数自动注入
 * <p>
 * 特定名称的缓存覆盖全局设置
 *
 * @author: wangyong
 * @date: 2020/1/9
 * <p>
 * @description:
 */

@Data
@Component
@ConditionalOnProperty(prefix = "cache.caffeine-plus", value = "enabled")
@ConfigurationProperties(prefix = "cache.caffeine-plus")
public class CacheProperties {

    private List<String> cacheName;
    private String spec;

    private List<Config> configs;

    @Getter
    @Setter
    public static class Config {
        private List<String> cacheName;
        private String spec;
    }

}


